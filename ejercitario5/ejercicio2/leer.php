<?php
require 'vendor/autoload.php';
require 'conexion.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

$nombreArchivo= 'pruebacomposer.xlsx';
$documento = IOFactory::load($nombreArchivo);

$totalHojas = $documento->getSheetCount();

//for ($indiceHoja = 0; $indiceHoja < $totalHojas; $indiceHoja ++){
    $hojaActual = $documento -> getSheet(0);
    $numeroFilas = $hojaActual->getHighestDataRow();
    $numeroColumna = Coordinate::columnIndexFromString($hojaActual->getHighestDataColumn());

    for ($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
        $consulta = "INSERT INTO alumnos (nombre, apellido, cedula, matricula, carrera, nacionalidad) VALUES (";
        for ($indiceColumna=1; $indiceColumna <= $numeroColumna; $indiceColumna++) {
            $valor = $hojaActual->getCellByColumnAndRow($indiceColumna,$indiceFila);
            if ($indiceColumna < $numeroColumna) {
                $consulta .= "'$valor', ";
            } elseif ($indiceColumna==$numeroColumna) {
                $consulta .="'$valor')";
            }
        }
        $result = $conexion->prepare($consulta);
        $result->execute();

    }

