<!DOCTYPE html>
<html lang="es">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- CSS only -->
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" href="style.css">
     <title>Ejercicio#5</title>
</head>
<body>
     <div class="container">
          <form action="" method="post" class="w-50 p-1 bg-light">
               <div class="mb-3">
                 <label for="usuario" class="form-label">Usuario</label>
                 <input type="text" class="form-control" name="usuario" id="usuario">
               </div>
               <div class="mb-3">
                 <label for="contrasenha" class="form-label">Contraseña</label>
                 <input type="password" class="form-control" name="contrasenha" id="contrasenha">
               </div>
               <button type="submit" class="btn btn-dark">Iniciar sesión</button>
          </form>
     </div>
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>