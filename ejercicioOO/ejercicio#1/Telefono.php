<?php
class Telefono
{
     private $tipo;
     private $numero;

     public function getTipo()
     {
          return $this->tipo;
     }
     public function getNumero()
     {
          return $this->numero;
     }

     public function setTipo($tipo)
     {
          $this->tipo = $tipo;
     }
     public function setNumero($numero)
     {
          $this->numero = $numero;
     }
}
