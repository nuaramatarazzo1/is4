<?php
class Persona
{
     private $nombre;
     private $apellido;
     private $cedula;
     private $direccion;
     private $edad;
     private $telefono = array();

     public function getNombre()
     {
          return $this->nombre;
     }
     public function getApellido()
     {
          return $this->apellido;
     }
     public function getCedula()
     {
          return $this->cedula;
     }
     public function getDireccion()
     {
          return $this->direccion;
     }
     public function getEdad()
     {
          return $this->edad;
     }
     /* telefono */
     public function getTelefono()
     {
          return $this->telefono;
     }

     public function setNombre($nombre)
     {
          $this->nombre = $nombre;
     }
     public function setApellido($apellido)
     {
          $this->apellido = $apellido;
     }
     public function setCedula($cedula)
     {
          $this->cedula = $cedula;
     }
     public function setDireccion($direccion)
     {
          $this->direccion = $direccion;
     }
     public function setEdad($edad)
     {
          $this->edad = $edad;
     }
     /* functions telefono */
     public function addTelefono($telefono)
     {
          array_push($this->telefono, $telefono);
     }
     public function setTelefono($index, $telefono)
     {
          $this->telefono[$index] = $telefono;
     }
}
