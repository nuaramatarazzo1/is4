<?php
include("Telefono.php");
include("Empresa.php");
include("Empleado.php");

$telefono1 = new Telefono();
$telefono1->setTipo('celular');
$telefono1->setNumero('0983334076');

$telefono2 = new Telefono();
$telefono2->setTipo('celular');
$telefono2->setNumero('0984789631');

$telefono3 = new Telefono();
$telefono3->setTipo('celular');
$telefono3->setNumero('0981592356');

$telefono4 = new Telefono();
$telefono4->setTipo('celular');
$telefono4->setNumero('0991256598');

$telefono5 = new Telefono();
$telefono5->setTipo('celular');
$telefono5->setNumero('0987825654');

$telefono6 = new Telefono();
$telefono6->setTipo('línea baja');
$telefono6->setNumero('021789456');

$telefono7 = new Telefono();
$telefono7->setTipo('línea baja');
$telefono7->setNumero('021598236');

$empleado1 = new Empleado();
$empleado1->setNombre('Juan');
$empleado1->setApellido('Fernández');
$empleado1->setCedula('5547895');
$empleado1->setDireccion('De los santos 456');
$empleado1->setEdad(25);
$empleado1->setCodigoEmpleado(1);
$empleado1->addTelefono($telefono1);

$empleado2 = new Empleado();
$empleado2->setNombre('Fernando');
$empleado2->setApellido('Pérez');
$empleado2->setCedula('4569821');
$empleado2->setDireccion('San Martín 556');
$empleado2->setEdad(30);
$empleado2->setCodigoEmpleado(2);
$empleado2->addTelefono($telefono2);

$empleado3 = new Empleado();
$empleado3->setNombre('Raúl');
$empleado3->setApellido('González');
$empleado3->setCedula('3245682');
$empleado3->setDireccion('San Pedro 1234');
$empleado3->setEdad(31);
$empleado3->setCodigoEmpleado(3);
$empleado3->addTelefono($telefono3);
$empleado3->addTelefono($telefono6);

$empleado4 = new Empleado();
$empleado4->setNombre('Fátima');
$empleado4->setApellido('Amarilla');
$empleado4->setCedula('5423581');
$empleado4->setDireccion('Panambi 236');
$empleado4->setEdad(22);
$empleado4->setCodigoEmpleado(4);
$empleado4->addTelefono($telefono4);
$empleado4->addTelefono($telefono5);
$empleado4->addTelefono($telefono7);

$empresa = new Empresa();
$empresa->setNombre('Ejemplo S.A.');
$empresa->setDireccion('Manuel Dominguez 456');
$empresa->addEmpleado($empleado1);
$empresa->addEmpleado($empleado2);
$empresa->addEmpleado($empleado3);
$empresa->addEmpleado($empleado4);

echo "<style>
          table{
               border-collapse: collapse;
          }
          td{
               border: 1px solid;
               padding: 2px 5px;
          }
     </style>";

echo '    Empresa: ' . $empresa->getNombre() . '<br>
          Dirección: ' . $empresa->getDireccion() . '<br><br>

          Lista de empleados<br>
          <table>
               <tr>
                    <td>Codigo de empleado</td>
                    <td>Nombre</td>
                    <td>Apellido</td>
                    <td>Cédula</td>
                    <td>Dirección</td>
                    <td>Edad</td>
                    <td>Teléfonos</td>
               </tr>
     ';
foreach ($empresa->getEmpleados() as $empleado) {
     echo '    
               <tr>
                    <td>' . $empleado->getCodigoEmpleado() . '</td>
                    <td>' . $empleado->getNombre() . '</td>
                    <td>' . $empleado->getApellido() . '</td>
                    <td>' . $empleado->getCedula() . '</td>
                    <td>' . $empleado->getDireccion() . '</td>
                    <td>' . $empleado->getEdad() . '</td>
                    <td> - 
          ';
     foreach ($empleado->getTelefono() as $telefono) {
          echo $telefono->getNumero() . ' - ';
     }
     echo '         </td>
               </tr>';
}
echo '    </table>';
