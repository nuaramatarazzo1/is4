<?php
include("Persona.php");

class Empleado extends Persona
{
     private $codigoEmpleado;

     public function getCodigoEmpleado()
     {
          return $this->codigoEmpleado;
     }

     public function setCodigoEmpleado($codigoEmpleado)
     {
          $this->codigoEmpleado = $codigoEmpleado;
     }
}
