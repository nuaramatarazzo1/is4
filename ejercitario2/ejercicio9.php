<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ejercicio9</title>
</head>
<body>
    <?php 
        $var = true;
        while ($var) {
            $numero = rand();
            if ($numero%983 === 0) {
                echo $numero . ' este número es divisible exactamente por 983';
                $var = false;
            }
        }
    ?>
</body>
</html>