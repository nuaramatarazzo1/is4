<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ejercicio4</title>
</head>
<body>
    <?php
        $variable = 24.5;
        echo nl2br(("tipo:".gettype($variable))."\n");
        $variable = 'HOLA';
        echo nl2br(("tipo:".gettype($variable))."\n");
        settype($variable, "integer");
        echo var_dump(("tipo:".gettype($variable))."\n");
    ?>
</body>
</html>