<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ejercicio7</title>
</head>
<body>
<?php
     $parcial1 = rand(0, 30);
     $parcial2 = rand(0, 20);
     $final1 = rand(0, 50);
     $acumulado = $parcial1 + $parcial2 + $final1;
     echo '$parcial1 = ' . $parcial1 . '<br>';
     echo '$parcial2 = ' . $parcial2 . '<br>';
     echo '$final1 = ' . $final1 . '<br>';
     echo '$acumulado = ' . $acumulado . '<br>';
     $nota;
     switch (true) {
          case ($acumulado > 0 && $acumulado < 60):
               $nota = 1;
               break;
          case ($acumulado > 59 && $acumulado < 70):
               $nota = 2;
               break;
          case ($acumulado > 69 && $acumulado < 80):
               $nota = 3;
               break;
          case ($acumulado > 79 && $acumulado < 90):
               $nota = 4;
               break;
          default:
               $nota = 5;
               break;
     }
     echo 'La nota es ' . $nota;
    ?>
</body>
</html>