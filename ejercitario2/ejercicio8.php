<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ejercicio8</title>
</head>
<body>
    <?php
        $contador = 0;
        while($contador < 900){
            $numero = rand(1, 10000);
            if ($numero%2 == 0) {
                echo $numero . '<br>';
                $contador++;
            }
        }
    ?>
</body>
</html>