<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ejercicio6</title>
</head>
<body>
    <form method='POST'>
            <label for='operando1'> Operando 1: </label>
            <input type='number'   name='operando1' id='operando1'>
            <label for='operando2'> Operando 2: </label>
            <input type='number'   name='operando2' id='operando2'>
            <br>
            <label for='select'>Seleccionar operación:</label>
            <select name='select'>
                <option>SUMAR</option>
                <option>RESTAR</option>
                <option>MULTIPLICAR</option>
                <option>DIVIDIR</option>
            </select>
            <br>
            <button type='submit' name="operandos">CALCULAR</button>
     </form>
        <?php
            if (isset($_POST['operandos']) && isset($_POST['operando1']) && isset($_POST['operando2']) && isset($_POST['select'])) {
                if (!empty($_POST['operando1']) && !empty($_POST['operando2'])) {
                     $operando1 = $_POST['operando1'];
                     $operando2 = $_POST['operando2'];
                     $operador = $_POST['select'];
                     $resultado;
                     switch ($operador) {
                          case 'SUMAR':
                               $resultado = $operando1 + $operando2;
                               break;
                          case 'RESTAR':
                               $resultado = $operando1 - $operando2;
                               break;
                          case 'MULTIPLICAR':
                               $resultado = $operando1 * $operando2;
                               break;
                          case 'DIVIDIR':
                               $resultado = $operando1 / $operando2;
                               break;
                     }
                     echo " <br>resultado: $resultado";  
                }
            }
        ?>  
      
</body>
</html>