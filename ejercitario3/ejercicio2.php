<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ejercicio2</title>
</head>
<body>
    <?php
        echo 'Versión actual de PHP: ' . phpversion() . '<br>';
        echo 'Versión ID actual de PHP: ' . PHP_VERSION_ID . '<br>';
        echo 'El valor máximo soportado en la version ' . PHP_VERSION_ID. ' para enteros es: ' . PHP_INT_MAX . '<br>';
        echo 'Tamaño máximo del nombre de un archivo: ' . PHP_MAXPATHLEN . '<br>';
        echo 'Versión del Sistema Operativo: ' . php_uname() . '<br>';
        echo 'Símbolo correcto de Fin De Línea para la plataforma en uso: ' . PHP_EOL . '<br>';
        echo 'Include path por defecto: ' . DEFAULT_INCLUDE_PATH . '<br>';
    ?>    
</body>
</html>