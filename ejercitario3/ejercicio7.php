<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ejercicio7</title>
</head>
<body>
    <?php
       
        function esPalindromo($cadena)
        {
            if (strlen($cadena)<2) {
                return false;
            }
        
            $cadena=strtolower(str_replace([" ", ",", "."], "", $cadena));
        
            for ($i=0;$i<strlen($cadena);$i++) {
                if ($cadena[$i]!=$cadena[strlen($cadena)-$i-1]) {
                    return false;
                }
            }
            return true;
        } 
        if (esPalindromo('radar')){
            echo "La palabra radar es palíndromo<br>";
        }
        if (esPalindromo('Neuquen')){
            echo "La palabra Neuquen, es palíndromo<br>";
        }
        if (esPalindromo('anilina,')){
            echo "La palabra anilina, es palíndromo<br>";
        }
        if (esPalindromo('anana,')){
            echo "La palabra anana, es palíndromo<br>";
        }
        if (esPalindromo('Malayalam,')){
            echo "La palabra Malayalam, es palíndromo<br>";
        }  
    ?>    
</body>
</html>