<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ejercicio3</title>
</head>
<body>
    <?php
        define('N', 7);
        $a = 1;
        echo '<table>';
        echo '<tr>';
        while ($a <= N) {
            if ($a%2 == 0) {
                echo '<td style="border:1px solid">';
                echo $a;
                echo '</td>';
            }
            $a++;
        }
        echo '</tr>';
        echo '</table>';
        
    ?>    
</body>
</html>